/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "security.h"
#include "security_parse_certificate.h"

#include <stdio.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

static security_app_t app;

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

amxd_dm_t* security_get_dm(void) {
    return app.dm;
}

amxo_parser_t* security_get_parser(void) {
    return app.parser;
}

static int import_certificate_from_default_location(void) {
    amxd_object_t* security_obj = NULL;
    int ret = -1;
    const char* locations_list_str = NULL;
    amxc_llist_t location_list;
    const char* location_str = NULL;
    amxc_string_t csv_locations;
    const amxc_var_t* locations_list_var = NULL;

    amxc_string_init(&csv_locations, 0);
    amxc_llist_init(&location_list);

    security_obj = amxd_dm_findf(app.dm, "Security.");
    when_null_trace(security_obj, exit, ERROR, "Failed to get the Security object");

    locations_list_var = amxd_object_get_param_value(security_obj, "CertificateLocation");
    when_null_trace(locations_list_var, exit, ERROR, "Failed to get the CertificateLocation parameter");

    locations_list_str = amxc_var_constcast(cstring_t, locations_list_var);
    when_null_trace(locations_list_str, exit, ERROR, "Failed to get the CertificateLocation parameter as a string");
    amxc_string_appendf(&csv_locations, "%s", locations_list_str);

    ret = (int) amxc_string_split_to_llist(&csv_locations, &location_list, ',');
    when_failed_trace(ret, exit, ERROR, "Failed to convert csv string to llist");
    amxc_llist_for_each(it, &location_list) {
        location_str = amxc_string_get(amxc_string_from_llist_it(it), 0);
        when_str_empty_trace(location_str, exit, ERROR, "Failed to get a string from the llist");
        ret = import_certificates(location_str);
        if(ret != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to import the certificates for path %s", location_str);
        }
    }
    ret = 0;
exit:
    amxc_string_clean(&csv_locations);
    amxc_llist_clean(&location_list, amxc_string_list_it_free);
    return ret;
}

static int security_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    app.dm = dm;
    app.parser = parser;

    return 0;
}

int _security_main(int reason,
                   amxd_dm_t* dm,
                   amxo_parser_t* parser) {
    int retval = 1;
    switch(reason) {
    case 0:     // START
        retval = security_init(dm, parser);
        when_false_trace(!retval, exit, ERROR, "Failed to init security_init");
        retval = import_certificate_from_default_location();
        when_false_trace(!retval, exit, ERROR, "Failed to get the certificates from the default location");
        break;
    case 1:     // STOP
        app.dm = NULL;
        app.parser = NULL;
        break;
    }
    retval = 0;
exit:
    return retval;
}

