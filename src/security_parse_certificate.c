/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "security_parse_certificate.h"

#include <amxd/amxd_transaction.h>
#include <amxp/amxp_dir.h>

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <libgen.h>

#include <openssl/x509v3.h>
#include <openssl/pem.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define PUB_KEY_MAX_LENGTH (1052 / 2) //hex to bytes

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

/*
   Currently we are not sure how the strings need to be parsed. So we are leaving it mostly how it gets returned from the X509 ssl library.
 */

static amxd_status_t get_cert_issuer(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    char* issuer = NULL;

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    issuer = X509_NAME_oneline(X509_get_issuer_name(cert), NULL, 0);
    when_null_trace(issuer, exit, ERROR, "Could not find the subject in the given cert");
    ret = amxd_trans_set_value(cstring_t, trans, "Issuer", issuer);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'Issuer' param in the transaction");
exit:
    free(issuer);
    return ret;
}

static amxd_status_t get_cert_subject(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    char* subj = NULL;

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    subj = X509_NAME_oneline(X509_get_subject_name(cert), NULL, 0);
    when_null_trace(subj, exit, ERROR, "Could not find the subject in the given cert");

    ret = amxd_trans_set_value(cstring_t, trans, "Subject", subj);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'Subject' param in the transaction");
exit:
    free(subj);
    return ret;
}

static amxd_status_t get_cert_serial_number(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    ASN1_INTEGER* serial_int = NULL;
    BIGNUM* bn = NULL;
    char* serial_str = NULL;

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    serial_int = X509_get_serialNumber(cert);
    when_null_trace(serial_int, exit, ERROR, "Could not find the serial number in the given cert");

    bn = ASN1_INTEGER_to_BN(serial_int, NULL);
    when_false_trace(bn, exit, ERROR, "Could not covert ASN1INTEGER to BN");

    serial_str = BN_bn2hex(bn);
    when_null_trace(serial_str, exit, ERROR, "Could not covert BN to string");

    ret = amxd_trans_set_value(cstring_t, trans, "SerialNumber", serial_str);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'SerialNumber' param in the transaction");
exit:
    OPENSSL_free(serial_str);
    BN_free(bn);
    return ret;
}

static amxd_status_t get_cert_signature_algorithm(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    int pkey_nid = -1;

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    pkey_nid = X509_get_signature_nid(cert);
    when_false_trace(pkey_nid != NID_undef, exit, ERROR, "Could not find signature algorithm name");
    ret = amxd_trans_set_value(cstring_t, trans, "SignatureAlgorithm", OBJ_nid2ln(pkey_nid));
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'SignatureAlgorithm' param in the transaction");
exit:
    return ret;
}

static amxd_status_t get_cert_validity_period(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    amxc_ts_t time;
    struct tm tm;

    amxc_ts_now(&time);

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    ret = ASN1_TIME_to_tm(X509_get0_notBefore(cert), &tm);
    when_false_trace(ret == 1, exit, ERROR, "Failed to convert the 'not before' valid period to 'struct tm'");
    time.sec = mktime(&tm);
    ret = amxd_trans_set_value(amxc_ts_t, trans, "NotBefore", &time);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'not before' param in the transaction");

    ret = ASN1_TIME_to_tm(X509_get0_notAfter(cert), &tm);
    when_false_trace(ret == 1, exit, ERROR, "Failed to convert the 'not after' valid period to 'struct tm'");
    time.sec = mktime(&tm);
    ret = amxd_trans_set_value(amxc_ts_t, trans, "NotAfter", &time);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'not after' param in the transaction");
exit:
    return ret;
}

static amxd_status_t get_cert_public_key(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    EVP_PKEY* pkey = NULL;
    int pkeyLen = -1;
    unsigned char* ucBuf = NULL;
    unsigned char* uctempBuf = NULL;
    amxc_string_t pub_key;
    int ret_val = -1;

    amxc_string_init(&pub_key, PUB_KEY_MAX_LENGTH);

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    pkey = X509_get_pubkey(cert);
    when_false_trace(pkey, exit, ERROR, "Could not find public key");

    pkeyLen = i2d_PublicKey(pkey, NULL);
    when_false_trace(pkeyLen <= PUB_KEY_MAX_LENGTH && pkeyLen >= 0, exit, ERROR, "Public key has to big of a size %d", pkeyLen);

    ucBuf = (unsigned char*) malloc(pkeyLen + 1);
    uctempBuf = ucBuf;
    ret_val = i2d_PublicKey(pkey, &uctempBuf);
    when_false_trace(ret_val >= 0, exit, ERROR, "Failed to excute i2d_PublicKey");
    for(int i = 0; i < pkeyLen; i++) {
        amxc_string_appendf(&pub_key, "%02x", (unsigned char) ucBuf[i]);
    }

    ret = amxd_trans_set_value(cstring_t, trans, "CAKey", amxc_string_get(&pub_key, 0));
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'CAKey' param in the transaction");
exit:
    free(ucBuf);
    EVP_PKEY_free(pkey);
    amxc_string_clean(&pub_key);
    return ret;
}

static amxd_status_t get_cert_subject_alts(X509_EXTENSION* ex, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    BIO* ext_bio = NULL;
    BUF_MEM* bptr = NULL;
    char* buf = NULL;

    when_null_trace(ex, exit, ERROR, "No valid ex parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    ext_bio = BIO_new(BIO_s_mem());

    when_false_trace(X509V3_EXT_print(ext_bio, ex, 0, 0), exit, ERROR, "Failed to pretty print the subject alts");

    BIO_get_mem_ptr(ext_bio, &bptr);
    buf = (char*) malloc((bptr->length + 1) * sizeof(char));
    memcpy(buf, bptr->data, bptr->length);
    buf[bptr->length] = '\0';

    ret = amxd_trans_set_value(cstring_t, trans, "SubjectAlt", buf);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'SubjectAlt' param in the transaction");
exit:
    BIO_free(ext_bio);
    free(buf);
    return ret;
}

static amxd_status_t handle_cert_extentions(X509* cert, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    X509_EXTENSION* ex = NULL;
    ASN1_OBJECT* obj = NULL;
    unsigned nid = NID_undef;
    int ext_count = -1;

    when_null_trace(cert, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    ext_count = X509_get_ext_count(cert);
    for(int i = 0; i < ext_count; i++) {
        ex = X509_get_ext(cert, i);
        when_null_trace(ex, exit, ERROR, "Failed to receive the extention");

        obj = X509_EXTENSION_get_object(ex);
        when_null_trace(ex, exit, ERROR, "Failed to receive the object");

        nid = OBJ_obj2nid(obj);
        if(nid == NID_subject_alt_name) {
            ret = get_cert_subject_alts(ex, trans);
            when_failed_trace(ret, exit, ERROR, "Could not get the subject alts");
        }
    }

    ret = amxd_status_ok;
exit:
    return ret;
}

static amxd_status_t get_cert_last_modification_date(const char* path_file, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    int ret_val = -1;
    struct stat attr;
    amxc_ts_t time;

    amxc_ts_now(&time);

    when_str_empty_trace(path_file, exit, ERROR, "No valid cert parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    ret_val = stat(path_file, &attr);
    when_false_trace(ret_val == 0, exit, ERROR, "Failed to excute stat");
    time.sec = attr.st_mtime;
    ret = amxd_trans_set_value(amxc_ts_t, trans, "LastModif", &time);
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'not after' param in the transaction");
exit:
    return ret;
}

static amxd_status_t get_cert_location(const char* name, amxd_trans_t* trans) {
    amxd_status_t ret = amxd_status_unknown_error;
    char* pathname = NULL;

    when_str_empty_trace(name, exit, ERROR, "No valid name parameter was given");
    when_null_trace(trans, exit, ERROR, "No valid trans parameter was given");

    pathname = strdup(name);
    ret = amxd_trans_set_value(cstring_t, trans, "CAFileName", basename(pathname));
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'not CAFileName' param in the transaction");
    free(pathname);

    pathname = strdup(name);
    ret = amxd_trans_set_value(cstring_t, trans, "CAPath", dirname(pathname));
    when_failed_trace(ret, exit, ERROR, "Failed to set the 'not CAPath' param in the transaction");
exit:
    free(pathname);
    return ret;
}

static int parse_certificate(const char* name, UNUSED void* priv) {
    amxd_status_t ret = amxd_status_unknown_error;
    FILE* fp = NULL;
    X509* cert = NULL;
    amxd_trans_t trans;
    amxd_object_t* security_obj = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_str_empty_trace(name, exit, ERROR, "No valid file name was given");

    security_obj = amxd_dm_findf(security_get_dm(), "Security.Certificate.");
    when_null_trace(security_obj, exit, ERROR, "Failed to get the Security.Certificate. object");

    ret = amxd_trans_select_object(&trans, security_obj);
    when_failed_trace(ret, exit, ERROR, "Failed to select the Security.Certificate. object");

    ret = amxd_trans_add_inst(&trans, 0, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to add the Security.Certificate. object to the transaction");

    fp = fopen(name, "r");
    when_null_trace(fp, exit, ERROR, "Could not open the file '%s'", name);

    cert = PEM_read_X509(fp, NULL, NULL, NULL);
    when_null_trace(cert, exit, WARNING, "Unable to parse certificate %s", name);

    ret = get_cert_serial_number(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the serial number of the certificate");

    ret = get_cert_issuer(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the issuer of the certificate");

    ret = get_cert_subject(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the subject of the certificate");

    ret = get_cert_signature_algorithm(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the signature algorithm of the certificate");

    ret = get_cert_validity_period(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the validity period of the certificate");

    ret = get_cert_public_key(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the public key of the certificate");

    ret = handle_cert_extentions(cert, &trans);
    when_failed_trace(ret, exit, ERROR, "Failed to handle the certificate extentions");

    ret = get_cert_last_modification_date(name, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the modification date of the certificate");

    ret = get_cert_location(name, &trans);
    when_failed_trace(ret, exit, ERROR, "Could not find the location");

    ret = amxd_trans_apply(&trans, security_get_dm());
    if(ret == amxd_status_duplicate) {
        SAH_TRACEZ_INFO(ME, "transaction did not add the instance, because the same instance already was added %s", name);
        ret = amxd_status_ok;
    }
    when_failed_trace(ret, exit, ERROR, "Failed to apply transaction to create a new certificate instance %s, %d", name, ret);
exit:
    X509_free(cert);
    if(fp != NULL) {
        fclose(fp);
    }
    amxd_trans_clean(&trans);
    return (int) ret;
}

int import_certificates(const char* name) {
    int ret = -1;

    when_str_empty_trace(name, exit, ERROR, "No valid file name was given");

    ret = amxp_dir_scan(name, "d_type == DT_REG", false, parse_certificate, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to go over the files in the dir '%s'. Check if this is a valid path", name);
exit:
    return ret;
}