# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.4.6 - 2024-07-23(09:18:02 +0000)

### Fixes

- Better shutdown script

## Release v0.4.5 - 2024-07-12(13:43:31 +0000)

### Other

- - Certificates are not read from autocert directory

## Release v0.4.4 - 2024-07-12(13:04:04 +0000)

### Other

- - Rework for use with OpenSSL3

## Release v0.4.3 - 2024-07-02(09:50:17 +0000)

### Other

- key-fix

## Release v0.4.2 - 2024-06-28(07:49:21 +0000)

### Other

- TR-181 Device.Security data model issues 19.03.2024

## Release v0.4.1 - 2024-04-10(07:11:54 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.4.0 - 2024-03-23(13:10:30 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.3.0 - 2023-10-16(10:26:56 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.2.3 - 2023-10-13(13:46:09 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.2.2 - 2023-06-20(08:27:25 +0000)

### Other

- Opensource component

## Release v0.2.1 - 2022-11-25(14:31:26 +0000)

### Fixes

- [TR181][security] increase public key size

## Release v0.2.0 - 2022-11-21(16:32:22 +0000)

### New

- [amx][prpl][TR181-Security] Develop a Device.Security plugin to handle Certificates.

## Release v0.1.0 - 2022-11-10(11:08:14 +0000)

### New

- [tr181][security] create dm of the security component

