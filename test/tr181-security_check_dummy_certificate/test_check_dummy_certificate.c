/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "test_check_dummy_certificate.h"
#include "security.h"
#include "../mocks/mock.h"
#include "test_utils.h"

void test_check_dummy_certificate(UNUSED void** state) {
    amxd_object_t* cert_inst = NULL;
    amxc_var_t security_var;
    amxd_status_t status = amxd_status_ok;
    const amxc_ts_t* time = NULL;
    char* path = NULL;

    amxc_var_init(&security_var);

    cert_inst = amxd_dm_findf(security_get_dm(), "Security.Certificate.1.");
    assert_non_null(cert_inst);
    status = amxd_object_get_params(cert_inst, &security_var, amxd_dm_access_private);
    assert_int_equal(status, 0);

    assert_string_equal(GET_CHAR(&security_var, "SerialNumber"), "604F1708C4F8CE79CD5DD168710217C120D316C6");
    assert_string_equal(GET_CHAR(&security_var, "Issuer"), "/CN=certificate.com/C=BE/ST=Flanders/L=Hoboken/O=axme");
    assert_string_equal(GET_CHAR(&security_var, "Subject"), "/CN=certificate.com/C=BE/ST=Flanders/L=Hoboken/O=axme");
    assert_string_equal(GET_CHAR(&security_var, "SubjectAlt"), "DNS:google.com, DNS:tesla.com, DNS:amazon.com, DNS:alphabet.com, DNS:be.be");
    assert_string_equal(GET_CHAR(&security_var, "SignatureAlgorithm"), "sha256WithRSAEncryption");
    assert_string_equal(GET_CHAR(&security_var, "CAFileName"), "test_cert.pem");
    assert_string_equal(GET_CHAR(&security_var, "CAKey"), "3082010a0282010100cdeeec7ea1721aee0f37aac5555073e723f5191f1b9c0d2df9abf9043d3aba59e2a212f9c8f7b7377713fb01aeeb9206e3d0097abf76d0516954ccd2814860ff364fd5c99318ab0dc21296c725bdbba7cd64e9edcbae6e5f61bc7887896050550f57b6fdfec80c3aa36d6bfda168549db30d000836d7a2f2c6387d1de6c128a33abde17d0e40abebbdba9c7c970f95920f0fdc301622532b440945d8b7452a5b5b0b307d502d26dcfd513f3a03d360928f251babed84ce0794fff9ab1f1b0eede3afce64c30f68033e7bff68d4b016b6a4a47f5c124a17b788184a09f09cb4f93b649d47b20d2d74d55e5dcbb8f3c8b13adf0ecb911b900a1544765f5c6226ad0203010001");
    path = strrchr(GET_CHAR(&security_var, "CAPath"), '/');
    assert_string_equal(path, "/dummy_certificates");

    time = amxc_var_constcast(amxc_ts_t, GET_ARG(&security_var, "LastModif"));
    assert_true(time->sec >= 1668417195);
    time = amxc_var_constcast(amxc_ts_t, GET_ARG(&security_var, "NotBefore"));
    assert_int_equal(time->sec, 1668417184);
    time = amxc_var_constcast(amxc_ts_t, GET_ARG(&security_var, "NotAfter"));
    assert_int_equal(time->sec, 1699953184);

    amxc_var_clean(&security_var);
}

